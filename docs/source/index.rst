.. swp-docs documentation master file, created by
   sphinx-quickstart on Thu Dec 22 10:47:24 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to swp-docs's documentation!
====================================

Here you will find documentation developed and used by the Scientific Software Platform team from the Champalimaud Foundation. Find more about us in the :ref:`about-label` section.


.. high level toc tree

.. toctree::
   :maxdepth: 2
   :includehidden:
   :caption: Tutorials

   Welcome <self>
   tutorials/docs-how-to/index
   tutorials/python-installation/index
   tutorials/pyqt-installation
   tutorials/opencv-installation

.. toctree::
   :maxdepth: 4
   :includehidden:
   :caption: About

   about/about

