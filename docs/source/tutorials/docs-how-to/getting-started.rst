.. _getting-started-label:

Getting started
=============================================

Install and configure sphinx
----------------------------

First, we need to create a folder to store our documentation. Normally, it should be in the project root folder.

You also need to install sphinx dependencies.

::

	mkdir docs
	pip install sphinx sphinx-autobuild
	pip install sphinx-rtd-theme # this is an optional theme


Then, you need to run the sphinx-quickstart script in order to generate the make files for sphinx. This script will prompt you with several questions. Below, we leave a suggestion of what you should answer

::

	cd docs
	sphinx-quickstart


Suggested answers:

	* > Separate source and build directories (y/n) [n]: **y**
	* > Do you want to use the epub builder (y/n) [n]: **y** (optional)
	* > autodoc: automatically insert docstrings from modules (y/n) [n]: **y**
	* > intersphinx: link between Sphinx documentation of different projects (y/n) [n]: **y**
	* > viewcode: include links to the source code of documented Python objects (y/n) [n]: **y**
	* > Create Makefile? (y/n) [y]: **y**
	* > Create Windows command file? (y/n) [y]: **y**

For all the other questions, leave the default answer or provide required information (e.g., your project name).


Changing the conf.py file
-------------------------
Any configuration related with sphinx should be inside the *conf.py* file which you should find in the */docs/source* folder.

For example, you can change the default theme by looking for the *html_theme* tag.

	:: 

		# The theme to use for HTML and HTML Help pages.  See the documentation for
		# a list of builtin themes.
		html_theme = 'sphinx_rtd_theme' # use Read The Docs theme


Edit, build, preview cycle
--------------------------
From now on, you can start working on your documentation. The process should go like this:

	* Make changes to your .rst files
	* Build your documentation: ::

		cd docs
		make clean # you only need to do this once a while
		make html

	* Preview the results by opening the file *docs/build/index.html*


.. seealso::

	You can find more information on this on the the following resources:

	http://www.sphinx-doc.org/en/1.5.1/tutorial.html
	https://docs.readthedocs.io/en/latest/getting_started.html#write-your-docs
	http://matplotlib.org/sampledoc/

	You can find projects using sphinx here:

	http://www.sphinx-doc.org/en/1.5.1/examples.html


