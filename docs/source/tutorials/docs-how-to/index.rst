Writing documentation with Sphinx
*********************************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting-started
   readthedocs
   source-code-documentation

