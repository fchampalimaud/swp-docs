On Mac OS
=========

Mac OS ships with a Python installation which is used by internal services. You should not mess up with this version. 

Homebrew (or just brew) is a package manager for Mac OS that offers a lot of useful libraries and software. We will use it to install Python on Mac OS.

Installing Homebrew
-------------------

You can install Python directly from Homebrew or to use Pyenv. Either way, you will need Homebrew installed. Just follow the instructions `here <http://brew.sh>`_.


Instaling Python with pyenv [recommended]
-----------------------------------------

If you want several Python versions installed at the same time, pyenv is what you need! 

pyenv lets you easily switch between multiple versions of Python. It's simple, unobtrusive, and follows the UNIX tradition of single-purpose tools that do one thing well.

.. note::

	* pyenv should be also available for other Unix systems.
	* You don't need to install Python with Homebrew, if you use pyenv.

How-to
^^^^^^

1. **Installing pyenv and pyenv-virtualenv using an Homebrew formula**

::

	brew install pyenv
	echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
	echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
	brew install pyenv-virtualenv
	echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bash_profile

	# show all installed versions
	pyenv versions

	# show current default python
	pyenv version

	# show all installed virtualenvs
	pyenv virtualenvs

.. note::

	You can still install a Python version using Homebrew. Pyenv does not conflict with that.

2. **Installing a python version and creating virtualenv**

::

	# choose whatever version you want here, the autocomplete helps
	# for advanced usage like creating executables, compiling packages, you need the python framework included
	env PYTHON_CONFIGURE_OPTS="--enable-framework" pyenv install 3.5.3

	# create new virtualenv with python version just created
	pyenv virtualenv 3.5.3 my-virtualenv-py3.5.3 

	# activate virtualenv
	pyenv activate my-virtualenv-py3.5.3

	# deactivate virtualenv
	pyenv deactivate

	# removing virtualenv
	pyenv uninstall my-virtualenv-py3.5.3

More info
^^^^^^^^^

.. note::

	* https://github.com/pyenv/pyenv

	* https://github.com/pyenv/pyenv-virtualenv

Installing Python from brew
---------------------------

If you don't like pyenv or just need only one version of Python3 or Python2 you can use Homebrew.


1. **Install Python3** using an Homebrew formula (also works for Python2)

::

	brew install python3

Optional scientific packages can be installed also with Homebrew. More information:

* [Scientific Python on Mac OS X 10.9+ with homebrew | Jörn's Blog](https://joernhees.de/blog/2014/02/25/scientific-python-on-mac-os-x-10-9-with-homebrew/)

* [Installing scientific Python on Mac OS X | Lowin Data Company](http://www.lowindata.com/2013/installing-scientific-python-on-mac-os-x/)

.. warning::

	These links may not be up-to-date. Please read carefully.
