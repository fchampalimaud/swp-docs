Publish on Read The Docs
========================

`Read The Docs <https://readthedocs.org>`_ (RTD) *hosts documentation, making it fully searchable and easy to find.*

To get started, create an account on RTD and import your project via a link to your git repository.

For simple projects (no complex dependencies), you don't need to configure much. RTD uses sphinx by default so it should be straightforward.

However, if your project needs to install several dependencies, you will need to install them via `pip <https://pypi.python.org/pypi/pip>`_ or `conda <https://pypi.python.org/pypi/conda>`_.

You can configure RTD in 2 ways:

	* configure RTD in the web interface and specify a requirements file (that should be in your project source code)
	* include a *readthedocs.yml* in your project source code root folder

Then you can specify what packages to install on the RTD environment via pip, conda, or both:

	* pip: specify the requirements file on the web interface or in the *readthedocs.yml*
	* conda: specify the path for the conda *environment.yml* in the *readthedocs.yml*
	* conda + pip: specify the path for the conda *environment.yml* in the *readthedocs.yml* and include pip dependencies

Below you will find an example of the third option which is the most flexible one.

::

	my_project
		docs # documentation folder
			build
			source
			environment.yml # conda settings
			Makefile
		myproject # source code folder
		readthedocs.yml # RTD settings

::

	# readthedocs.yml

	conda:
	    file: docs/environment.yml


::

	# environment.yml

	name: my-project-env # this is just a name for the environment
	dependencies:
	  - sphinx_rtd_theme
	  - python=3.5
	  - pyqt=4
	  - numpy
	  - pip:
	      - sphinx
	      - recommonmark
	      - https://bitbucket.org/fchampalimaud/logging-bootstrap/get/master.zip

.. seealso::

	You can find more information on this on the the following resources:

	https://docs.readthedocs.io/en/latest/
	https://docs.readthedocs.io/en/latest/getting_started.html#write-your-docs

	http://conda.pydata.org/docs/building/meta-yaml.html#the-meta-yaml-file
	https://docs.readthedocs.io/en/latest/conda.html?highlight=conda
