Installing PyQT
***************

PyQt5 installation with QScintilla2 using pip
=============================================

PyQt5 installation is very easy and cross-platform. Just use pip as stated below.

::

	pip install pyqt5
	pip install qscintilla


Installing PyQt5 on Mac OS with brew [DEPRECATED]
=================================================

.. warning::

	PyQt5 and QScintilla2 can now be installed via PIP which is much easier.


1. Uninstall Qt4 (optional)

::

	brew uninstall pyqt
	brew uninstall qt
	brew uninstall sip
	brew uninstall qscintilla2 # optional
	brew uninstall opencv3 # optional
	brew untap cartr/qt4 # if installed

2. Install Qt5, QScintilla2 and OpenCV3

::

	# install pyqt5
	brew install pyqt5 --with-python

	# install qscintilla2 with qt5 support
	brew install qscintilla2 --with-plugin --with-python


PyQt4 Installation with QScintilla [DEPRECATED]
===============================================

On Mac OS
---------

Homebrew no longer officially supports Qt4. Everything by default uses Qt5. This is an alternative method to install Qt4 with QScintilla.

.. warning::
	You should remove any version of Qt5 and PyQt5 installed via Homebrew.

	Upgrading QScintilla may overwrite Qt4 installation.


1. **Install Qt4**

::

	brew tap cartr/qt4
	brew install cartr/qt4/qt

2. **Install PyQt** using an old Homebrew formula

::

	brew install pyqt.rb --with-python3

:download:`pyqt.rb </_attachments/pyqt.rb>`


.. note::
	This file is an old version of Homebrew for PyQt4. You can update some details if you want in order to adequate to your system (e.g. Mac OS version, pyqt version).


3. **Install QScintilla** using an old Homebrew formula

::

	brew install qscintilla2.rb --with-python3

:download:`qscintilla2.rb </_attachments/qscintilla2.rb>`


.. note::

	This file is an old version of Homebrew for PyQt4. You can update some details if you want in order to adequate to your system (e.g. Mac OS version, qscintilla version).



