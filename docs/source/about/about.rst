.. _project-info-label:

************
The SWP Team
************

.. image:: /_images/fc_logo.jpg
	:scale: 50 %

`Scientific Software Platform (Champalimaud Foundation) <http://research.fchampalimaud.org/en/research/platforms/staff/Scientific%20Software/>`_

The Scientific Software Platform (SWP) from the Champalimaud Foundation provides technical know-how in software engineering and high quality software support for the Neuroscience and Cancer research community at the Champalimaud Foundation.

We typical work on computer vision / tracking, behavioral experiments, image registration and database management.

Members
=======

The current and former members of the **pybpod-api** team.

* `@UmSenhorQualquer <https://github.com/UmSenhorQualquer/>`_ Ricardo Ribeiro
* `@JBauto <https://github.com/JBauto>`_ João Baúto
* `@cajomferro <https://github.com/cajomferro/>`_ Carlos Mão de Ferro **[former member]**

.. image:: /_images/team2017.jpg
	:scale: 50 %

License
=======
We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`_.


Questions?
==========
If you have any questions or want to report a problem with this library please fill a issue `here <https://bitbucket.org/fchampalimaud/pybpod-api/issues>`_.


.. Changes log
.. -----------

.. TODO
