Installing OpenCV for Python development
****************************************

On Mac OS
=========

Installing Opencv3 with qt5, ffmpeg and opengl support
------------------------------------------------------

::

	# install opencv3 with qt5, ffmpeg and opengl support
	brew install opencv3 --with-python3 --with-qt --with-ffmpeg --with-opengl

	# export file to your python installation
	echo /usr/local/opt/opencv3/lib/python3.6/site-packages >> /usr/local/lib/python3.6/site-packages/opencv3.pth


On Windows
==========

Installing Opencv3 with qt5, ffmpeg and opengl support
------------------------------------------------------

If you use Python2 with WinPython, you can download the official OpenCV binaries.

1. Install opencv310 from http://opencv.org/releases.html
2. Copy opencv\\build\\python\\x86\\2.7\\cv2.pyd to WinPython-32bit-2.7.10.3\\python-2.7.10\\Lib\\site-packages
3. Copy opencv_ffmpeg310.dll to WinPython-32bit-2.7.10.3\python-2.7.10

For python3, you need to install the unofficial wheel: http://www.lfd.uci.edu/~gohlke/pythonlibs/.



