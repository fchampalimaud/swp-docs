
On Windows 
==========

Using WinPython [recommended]
-----------------------------

Installing Python on Windows can be simpler if you use `WinPython <http://winpython.github.io>`_. This way you do not mess with other Python installations on the system.

You can download the latest version here: https://sourceforge.net/projects/winpython/files/latest/download?source=files
