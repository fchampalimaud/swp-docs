Experiment
=================================================

::

    Experiment = type(
        'Experiment',
        tuple(conf.GENERIC_EDITOR_PACKAGES_FINDER.find_class('models.experiment.Experiment') + [ExperimentUIBusy]),
        {}
    )


.. inheritance-diagram:: pycontrolgui.models.experiment.experiment_uibusy
    :parts: 1

.. automodule:: pycontrolgui.models.experiment.experiment_window
    :members:
    :show-inheritance:
    :private-members:

.. automodule:: pycontrolgui.models.experiment.experiment_treenode
    :members:
    :show-inheritance:
    :private-members:

.. automodule:: pycontrolgui.models.experiment.experiment_dockwindow
    :members:
    :show-inheritance:
    :private-members:

.. automodule:: pycontrolgui.models.experiment.experiment_uibusy
    :members:
    :show-inheritance:
    :private-members:

